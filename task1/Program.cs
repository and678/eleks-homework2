﻿using System;
using System.Collections.Generic;
using task1.Classes;
using task1.Interfaces;

namespace HomeWork1
{
	class Program
	{
		public static void Main()
		{
			List<Author> authorArray = new List<Author> {
				new Author("Petro"),
				new Author("Andriy"),
				new Author("Mukola")
			};
			Library test1 = new Library("Biblioteka", new List<Category> {
				new Category("Science", new List<Book> {
					new Book("Math", authorArray[0], 50),
					new Book("Physics", authorArray[1], 555),
					new Book("C# Programming", authorArray[2], 65)
				}),
				new Category("Fiction", new List<Book> {
					new Book("Fahrenheit 451", authorArray[0], 45)
				}),
				new Category("other", new List<Book> {
					new Book("No books", authorArray[1], 352),
					new Book("sorry", authorArray[2], 255)
				})
			});

			ISerializationHelper json = new jsonLibrarySerializator();

			json.Write(test1, @"jsonLib.json");

			json.Read(@"jsonLib.json", out authorArray).Display();

			/*Console.WriteLine("Sorting books in categories test.\nBefore sorting: ");
			test1.Display();
			foreach(var cat in test1.Categories) {
				cat.SortByPages();
			}
			Console.WriteLine("\nAfter:");
			test1.Display();
			Console.WriteLine("\nSearch book on c#:");
			var searchResult = authorArray[0].FindBook("C# Programming");
			if (searchResult != null) {
				Console.WriteLine($"\tBook: {searchResult.Name} was found. Author: {searchResult.Author.Name}.");
			}


			Console.WriteLine("\nWrite to xml file, edit, and read back:");

			ISerializationHelper serializator = new XmlLibrarySerializator();

			serializator.Write(test1, "myLibrary.xml");

			serializator.DeleteBook("myLibrary.xml", "Math");
			serializator.CapitalizeAllBooks("myLibrary.xml");

			Library test2 = serializator.Read("myLibrary.xml", out authorArray);
			test2.Display();*/

			Console.Read();
		}
	}
}