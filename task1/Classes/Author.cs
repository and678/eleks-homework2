﻿using System;
using System.Collections.Generic;
using System.Linq;
using task1.Interfaces;

namespace task1.Classes
{

	public class Author : IAuthor, IComparable<Author>
	{
		public string Name { get; set; }
		public List<Book> Books { get; set; }

		public Author()
		{
			Name = "";
			Books = new List<Book>();
		}

		public Author(string newName)
		{
			Name = newName;
			Books = new List<Book>();
		}

		public int CompareTo(Author other)
		{
			return this.Books.Count.CompareTo(other.Books.Count);
		}

		public int CountBooks()
		{
			return Books.Count;
		}

        public Book FindBook(string bookName)
        {
			Console.WriteLine(Name + " - " + bookName);
            var result = Books.Where(n => n.Name == bookName);
			if (result.Any())
			{
				return result.First();
			}
            return null;
        }
    }
}