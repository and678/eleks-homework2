﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task1.Interfaces;

namespace task1.Classes
{
	class jsonLibrarySerializator : ISerializationHelper
	{
		public Library Read(string fileName, out List<Author> authors)
		{
			authors = new List<Author>();
			Library result = new Library();
			using (StreamReader reader = File.OpenText(fileName))
			{
				JObject JsonLibrary = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
				result.Name = JsonLibrary.GetValue("name").ToString();
				foreach (var cat in JsonLibrary.GetValue("categories"))
				{
					Category currentCat = new Category();
					currentCat.Name = ((JObject)cat).GetValue("name").ToString();
					foreach (var book in ((JObject)cat).GetValue("books"))
					{
						Book currentBook = new Book();
						currentBook.Name = ((JObject)book).GetValue("name").Value<string>();
						currentBook.Pages = ((JObject)book).GetValue("pages").Value<int>();
						Author currentAuthor = authors.Find(n => (n.Name == ((JObject)book).GetValue("author").Value<string>()));
						if (currentAuthor == null)
						{
							currentAuthor = new Author(((JObject)book).GetValue("author").Value<string>());
							authors.Add(currentAuthor);
						}

						currentBook.Author = currentAuthor;


						currentCat.Books.Add(currentBook);
					}
					result.Categories.Add(currentCat);
				}
			}
			return result;
		}

		public void Write(Library input, string fileName)
		{
			JObject JsonLibrary =
				new JObject(
					new JProperty("name", input.Name),
					new JProperty("categories",
						new JArray(
							from cat in input.Categories
							select new JObject(
								new JProperty("name", cat.Name),
								new JProperty("books",
									new JArray(
										from book in cat.Books
										select new JObject(
											new JProperty("name", book.Name),
											new JProperty("author", book.Author.Name),
											new JProperty("pages", book.Pages.ToString()))))))));
			File.WriteAllText(fileName, JsonLibrary.ToString());
		}
	}
}
