﻿using System.Xml.Linq;
using System.Xml;
using System;
using System.Collections.Generic;
using task1.Interfaces;

namespace task1.Classes
{
	class XmlLibrarySerializator : ISerializationHelper
	{
		public void Write(Library input, string fileName)
		{
			XElement xLibrary = new XElement("Library", new XAttribute("name", input.Name));
			//xLibrary.SetAttributeValue("name", input.Name);
			foreach (Category cat in input.Categories)
			{
				XElement xCategory = new XElement("Category");
				xCategory.SetAttributeValue("name", cat.Name);
				foreach(Book bk in cat.Books)
				{
					XElement xBook = new XElement("Book");
					xBook.SetAttributeValue("name", bk.Name);
					xBook.SetAttributeValue("pages", bk.Pages);
					xBook.SetAttributeValue("author", bk.Author.Name);
					xCategory.Add(xBook);
				}
				xLibrary.Add(xCategory);
			}
			XDocument file = new XDocument(xLibrary);
			file.Save(fileName);
		}


		public Library Read(string fileName, out List<Author> authors)
		{
			XDocument xDoc = XDocument.Load(fileName);
			authors = new List<Author>();
			if (xDoc != null && xDoc.Root.HasElements)
			{
				XElement xLibrary = xDoc.Element("Library");
				Library result = new Library();
				result.Name = xLibrary.Attribute("name").Value;

				foreach (var xCategory in xLibrary.Elements("Category"))
				{
					Category currentCat = new Category();
					currentCat.Name = xCategory.Attribute("name").Value;
					foreach (var xBook in xCategory.Elements("Book"))
					{
						Book currentBook = new Book();
						currentBook.Name = xBook.Attribute("name").Value;
						currentBook.Pages = Int32.Parse(xBook.Attribute("pages").Value);

						Author currentAuthor = authors.Find(n => (n.Name == xBook.Attribute("author").Value));
						if (currentAuthor == null)
						{
							currentAuthor =  new Author(xBook.Attribute("author").Value);
							authors.Add(currentAuthor);
						}

						currentBook.Author = currentAuthor;
						currentAuthor.Books.Add(currentBook);
						
						currentCat.Books.Add(currentBook);
					}
					result.Categories.Add(currentCat);
				}
				return result;
			}
			return null;
		}
		public void CapitalizeAllBooks(string fileName)
		{
			XDocument xDoc = XDocument.Load(fileName);
			if (xDoc != null && xDoc.Root.HasElements)
			{
				XElement xLibrary = xDoc.Element("Library");
				foreach (var xCategory in xLibrary.Elements("Category"))
				{
					foreach (var xBook in xCategory.Elements("Book"))
					{
						xBook.Attribute("name").SetValue(xBook.Attribute("name").Value.ToUpper());
					}
				}
				xDoc.Save(fileName);
				return;
			}
		}
		public void DeleteBook(string fileName, string bookName)
		{
			XDocument xDoc = XDocument.Load(fileName);
			if (xDoc != null && xDoc.Root.HasElements)
			{
				XElement xLibrary = xDoc.Element("Library");
				foreach (var xCategory in xLibrary.Elements("Category"))
				{
					foreach (var xBook in xCategory.Elements("Book"))
					{
						if (xBook.Attribute("name").Value == bookName)
						{
							xBook.Remove();
							xDoc.Save(fileName);
							return;
						}
					}
				}
			}
		}
	}
}
