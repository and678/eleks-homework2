﻿using System;
using System.Collections.Generic;
using task1.Interfaces;
namespace task1.Classes
{
	public class Library : ICountingBooks, IComparable<Library>
	{
		public string Name { get; set; }

		public List<Category> Categories { get; set; }

		public Library()
		{
			Name = "";
			Categories = new List<Category>();
		}

		public Library(string newName, List<Category> newCats)
		{
			Name = newName;
			Categories = newCats;
		}

		public int CompareTo(Library other)
		{
			return Categories.Count.CompareTo(other.Categories.Count);
		}

		public int CountBooks()
		{
			throw new NotImplementedException();
		}

		public override string ToString()
		{
			return $"Library: {Name}. Has {Categories.Count} categories.";
		}
		public void Display()
		{
			Console.WriteLine($"Library name: {Name}");
			foreach (var cat in Categories) {
				Console.WriteLine("\t" + $"Category name: {cat.Name}");
				foreach (var book in cat.Books)
				{
					Console.WriteLine("\t\t" + $"Book name: {book.Name}, author: {book.Author.Name}, pages: {book.Pages}.");
				}
			}
		}
		
	}
}