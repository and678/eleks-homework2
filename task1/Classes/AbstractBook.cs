﻿using System;

namespace task1.Classes
{
	public abstract class AbstractBook
	{
		public string Name { get; set; }

		public int Pages { get; set; }


		public Author Author { get; set; }
		public AbstractBook() : this("")
		{
		}

		public AbstractBook(string newName, Author newAuthor = null, int newPages = 0)
		{
			Name = newName;
			this.Author = newAuthor;
			Pages = newPages;
		}
	}
}