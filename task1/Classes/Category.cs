﻿using System;
using System.Collections.Generic;
using System.Linq;
using task1.Interfaces;
namespace task1.Classes
{
	public class Category : ICountingBooks, IComparable<Category>
	{
		public string Name { get; set; }

		public List<Book> Books { get; private set; }

		public Category()
		{
			Name = "";
			Books = new List<Book>();
		}

		public Category(string newName, List<Book> newBooks)
		{
			Name = newName;
			Books = newBooks;
		}

		public int CompareTo(Category other)
		{
			return Books.Count.CompareTo(other.Books.Count);
		}

		public int CountBooks()
		{
			return Books.Count;
		}

		public override string ToString()
		{
			return $"Library: {Name}. Has {Books.Count} books.";
		}

		public void SortByPages()
		{
			Books = Books.OrderBy(b => b.Pages).ToList<Book>();
		}

		public Book FindBook(string bookName)
		{
			var result = Books.Where(n => n.Name == bookName);
			if (result.Any())
			{
				return result.First();
			}
			return null;
		}
	}
}