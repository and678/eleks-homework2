﻿using System;
using System.Collections.Generic;
using task1.Classes;

namespace task1.Interfaces
{
	interface IAuthor : ICountingBooks
	{
		String Name { get; set; }
		List<Book> Books { get; set; }
        Book FindBook(string bookName);
	}
}