﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task1.Classes;

namespace task1.Interfaces
{
	interface ISerializationHelper
	{
		void Write(Library input, string fileName);
		Library Read(string fileName, out List<Author> authors);
	}
}
